#!/usr/bin/env ruby
# -*- coding:utf-8 -*-
require 'bundler'
Bundler.require
require_relative 'lib/client'

CLIENT_ID     = '578ca9f7c6fbb2fb65f0369e9c6d6000'
CLIENT_SECRET = '97d9180e248ad72248f61920903124e4'
USER_NAME     = ENV['SOUNDCLOUD_USER_NAME']
USER_PASSWORD = ENV['SOUNDCLOUD_USER_PASSWORD']
TARGET_USERS  = %w[
  user18081971
  richarddjames
  yoshimototakaaki
]

def wait sec = 60
  str_len = sec.to_s.length
  sec.times do |i|
    print "%#{str_len}d" % (sec - i)
    sleep 1
    print "\033[2K\033[#{str_len}D"
  end
end

def save_filename sound
  "#{__dir__}/#{sound[:user]}/#{sound[:permalink]} #{sound[:title].gsub %r{[<>:"/\\|?*]}, '_'}.#{sound[:ext]}"
end

client = Client.new
TARGET_USERS.each do |target_user|
  FileUtils.mkdir_p "#{__dir__}/#{target_user}"
  sounds = client.download_urls target_user
  no_dl_sounds = sounds.select{|sound| !File.exist? save_filename sound }
  puts "Detect #{no_dl_sounds.length}/#{sounds.length} files not DLed."
  no_dl_sounds.each_with_index do |sound, i|
    filename = save_filename sound
    message = "Downloading #{sound[:title]} ...(#{i + 1}/#{no_dl_sounds.length})"
    print message
    client.download sound, filename
    puts "\033[2K\033[#{message.length}DDonwloaded #{File.basename filename}!"
    wait 30 if i + 1 < no_dl_sounds.length
  end
end
