<?php
$filename = __DIR__.preg_replace('#(\?.*)$#', '', urldecode($_SERVER['REQUEST_URI']));
if (PHP_SAPI === 'cli-server' && is_file($filename)) {
    return false;
}

function h($str) {
  echo htmlspecialchars($str, ENT_QUOTES|ENT_DISALLOWED|ENT_HTML5, 'UTF-8');
}

$targetDirs = [];
$dir = opendir(__DIR__);
while (false !== ($entry = readdir($dir))) {
  if (is_dir($entry) && '.' !== $entry[0] && 'lib' !== $entry) {
    $targetDirs[] = $entry;
  }
}
closedir($dir);
$soundSets = array_reduce(
  $targetDirs,
  function ($soundSets, $dir) {
    $soundSets[$dir] = [];
    foreach (scandir(__DIR__.'/'.$dir) as $entry) {
      if (is_file(__DIR__."/$dir/$entry")) {
        $soundSets[$dir][] = $entry;
      }
    }
    return $soundSets;
  },
  []
);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8"/>
  <title>sc-backupper</title>
  <style>
body {
  font-family : 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, 'ヒラギノ角ゴ ProN W3', 'Hiragino Kaku Gothic ProN', Meiryo, sans-serif;
  font-size   : 9pt;
}
  </style>
</head>
<body>
  <div id="player">
    <select class="player_dir"></select>
    <div class="player_controller">
      <div class="player_controller_btn player_controller_play">[Play› ]</div>
      <div class="player_controller_btn player_controller_btn-disabled player_controller_pause">[Pause‖ ]</div>
      <div class="player_controller_btn player_controller_next">[Next» ]</div>
    </div>
    <div class="player_title"></div>
  </div>
  <script>
(function () {
'use strict';
var soundSets = JSON.parse(decodeURIComponent("<?php echo rawurlencode(json_encode($soundSets)); ?>"));

// {{{ EventEmitter
function EventEmitter() {
  this.listeners = {};
}

EventEmitter.prototype.emit = function (name, params) {
  var i = 0, iz = 0,
      listeners = this.listeners[name];
  params = params || [];
  for (i = 0, iz = listeners.legth; i < iz; ++i) {
    listeners[i].apply(this, params);
  }
};

EventEmitter.prototype.on = function (name, listener) {
  if (!this.listeners[name]) {
    this.listeners[name] = [];
  }
  this.listeners[name].push(listener);
};
// }}}

// {{{ Player
function Player() {
  if (Player.instance) {
    return Player.instance;
  }
  if (!(this instanceof Player)) {
    return new Player();
  }
  EventEmitter.call(this);
  this.audio = new Audio();
}

Player.instance = null;

Player.prototype = Object.create(EventEmitter.prototype);
Player.prototype.constructor = Player;

Player.prototype.play = function (url) {
};

Player.prototype.pause = function () {
};
// }}}

// {{{ UiPlayer
function UiPlayer(soundSets) {
  var nodeControllerPlay, nodeControllerPause,
      me = this;
  EventEmitter.call(this);
  this.currentDir   = '';
  this.currentTitle = '';
  this.playing      = false;
  this.node         = document.getElementById('player');
  this.nodeDir      = this.querySelector('.player_dir');
  this.nodeTitle    = this.node.querySelector('.player_title');
  this.nodeDir.appendChild(
    Object.keys(soundSets).reduce(function (fragment, dir, i) {
      var option = document.createElement('option');
      option.value = option.textContent = dir;
      if (0 === i) {
        option.selected = true;
        me.currentDir = dir;
        me.currentTitle = soundSets[dir][0];
      }
      fragment.appendChild(option);
      return fragment;
    }, document.createDocumentFragment())
  );
  this.nodeDir.addEventListener('change', function () {
    if (me.currentDir === me.nodeDir.value) { return; }
    me.currentDir = me.nodeDir.value;
  });
  nodeControllerPlay  = this.node.querySelector('.player_controller_play');
  nodeControllerPause = this.node.querySelector('.player_controller_pause');
  nodeControllerPlay.addEventListener('click', function () {
    if (me.playing) { return; }
    me.playing = true;
  });
  nodeControllerPause.addEventListener('click', function () {
    if (!me.playing) { return; }
    me.playing = false;
  });
  this.node.querySelector('.player_controller_next').addEventListener('click', function () {
  });
  this.on('dblclickSound', function (sound) {
  });
  parentNode.appendChild(this.node);
}

UiPlayer.prototype = Object.create(EventEmitter.prototype);
UiPlayer.prototype.constructor = UiPlayer;
// }}}

window.addEventListener('DOMContentLoaded', function () {
  new UiPlayer(soundSets);
});

}());
  </script>
</body>
</html>
<!-- vim:set fdm=marker : -->
