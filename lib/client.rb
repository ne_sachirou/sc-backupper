# SoundCloud client.
class Client
  def initialize
    @client = SoundCloud.new client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      username: USER_NAME,
      password: USER_PASSWORD
  end

  def download_urls user, offset = 0, urls = []
    limit = 200
    tracks = @client.get "/users/#{user}/tracks",
      limit: limit,
      offset: offset,
      linked_partitioning: 1
    urls += tracks.collection.
      select{|t| t.downloadable || t.streamable }.
      collect do |t|
        {
          title:        t.title,
          ext:          t.original_format,
          downloadable: t.downloadable,
          url:          t.downloadable ? t.download_url : t.stream_url,
          user:         t.user.permalink,
          permalink:    t.permalink,
        }
      end
    if nil == tracks.next_href
      urls
    else
      download_urls user, offset + limit, urls
    end
  end

  def download url_data, filename
    open filename, 'wb' do |f|
      if url_data[:downloadable]
        f.write @client.get URI(url_data[:url]).path
      else
        uri = URI url_data[:url]
        uri.query = URI.encode_www_form client_id: CLIENT_ID
        save_stream uri, f
      end
    end
    raise Exception.new if 0 == File.size(filename)
    Mp3Info.open(File.realpath filename) do |mp3|
      mp3.tag.title = url_data[:title]
    end
  rescue => ex
    File.delete filename rescue nil
    raise ex
  end

  private

  def save_stream uri, file
    Net::HTTP.get_response uri do |res|
      case res
      when Net::HTTPSuccess
        res.read_body{|segment| file.write segment }
      when Net::HTTPRedirection
        save_stream URI(res['location']), file
      else
        raise res
      end
    end
  end
end
